//
//  TableViewCell.h
//  SchoolManagement
//
//  Created by Macwaves on 4/15/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell


@property(nonatomic,strong) IBOutlet UILabel *nameLabel;

@property(nonatomic,strong) IBOutlet UITextField *textfield;


@end
