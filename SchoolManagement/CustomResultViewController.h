//
//  CustomResultViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 4/16/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudentDetails.h"
#import "Database.h"
#import "CustomTextField.h"

@class TPKeyboardAvoidingScrollView;

@protocol MJSecondPopupDelegate;
@interface CustomResultViewController : UIViewController

@property (assign, nonatomic) id <MJSecondPopupDelegate>delegate;

@property(nonatomic,strong) StudentDetails *studentInfo;

@property(nonatomic,strong) Database *studentData;

@property(nonatomic,strong) IBOutlet CustomTextField  *maths,*physics,*chem,*hindi,*english;

@property(nonatomic,strong) IBOutlet UILabel *Name;

@property (nonatomic, retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

-(IBAction)upDat:(id)sender;

-(IBAction)Canc:(id)sender;

@end

@protocol MJSecondPopupDelegate<NSObject>
@optional
- (void)cancelButtonClicked:(CustomResultViewController*)secondDetailViewController;

@end
