//
//  CustomButton.m
//  SchoolManagement
//
//  Created by Macwaves on 4/13/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton

-(void)awakeFromNib{
    self.layer.cornerRadius=5.0f;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
