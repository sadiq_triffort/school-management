//
//  CustomResultViewController.m
//  SchoolManagement
//
//  Created by Macwaves on 4/16/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.


#import "CustomResultViewController.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface CustomResultViewController ()<MJSecondPopupDelegate>

@end

@implementation CustomResultViewController
@synthesize studentInfo;

#pragma mark DidLoad
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.scrollView.layer.cornerRadius=8.0f;
    NSString *imageName = @"background_815.jpg";
    
    self.scrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:imageName]]];
    
    _Name.text=studentInfo.name;
    
    self.studentData = [[Database alloc] init];
    
    [self.studentData initDatabase];
    
    [self textFieldValues];
    
    [self.scrollView contentSizeToFit];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark  Fill Value to text field
-(void)textFieldValues{
    
    _maths.text = studentInfo.maths;
    _physics.text=studentInfo.physics;
    _chem.text=studentInfo.chemistry;
    _hindi.text=studentInfo.hindi;
    _english.text=studentInfo.english;
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark IBAction  Update Marks
-(IBAction)upDat:(id)sender{
    
    studentInfo.maths=_maths.text;
    
    studentInfo.physics=_physics.text;
    
    studentInfo.chemistry=_chem.text;
    
    studentInfo.hindi=_hindi.text;
    
    studentInfo.english=_english.text;
    
    [_studentData addResults:studentInfo];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
    
}

-(IBAction)Canc:(id)sender{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
