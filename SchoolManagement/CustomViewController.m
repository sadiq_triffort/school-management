//
//  CustomViewController.m
//  SchoolManagement
//
//  Created by Macwaves on 4/15/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "CustomViewController.h"
#import "TableViewCell.h"
#import "CustomTextField.h"
#import "UIViewController+MJPopupViewController.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface CustomViewController ()<MJSecondPopupDelegate>{
    
}

@end

@implementation CustomViewController
@synthesize studentInfo,delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.layer.cornerRadius=8.0f;
    
    self.studentData = [[Database alloc] init];
    
    [self.studentData initDatabase];
    [self textFieldValues];
    NSString *imageName = @"background_815.jpg";
    
    self.scrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:imageName]]];
    
     [self.scrollView contentSizeToFit];
    // Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(void)textFieldValues{
    
    _name.text = studentInfo.name;
    _f_name.text=studentInfo.fathersName;
    _eid.text=studentInfo.EmailIdStudent;
    _s_phn.text=studentInfo.s_phn;
    _f_pn.text=studentInfo.f_phn;
    _s_add.text=studentInfo.s_Address;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)upDate:(id)sender{
    
    studentInfo.name=_name.text;
    studentInfo.fathersName=_f_name.text;
    studentInfo.EmailIdStudent=_eid.text;
    studentInfo.s_phn=_s_phn.text;
    studentInfo.f_phn=_f_pn.text;
    studentInfo.s_Address=_s_add.text;
    [_studentData updateStudent:studentInfo];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }

    
}

-(IBAction)Cancel:(id)sender{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
