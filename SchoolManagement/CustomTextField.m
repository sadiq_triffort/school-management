//
//  CustomTextField.m
//  SchoolManagement
//
//  Created by Macwaves on 4/13/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

-(void)awakeFromNib{
    self.layer.cornerRadius=5.0f;
    self.backgroundColor=[UIColor lightGrayColor];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
