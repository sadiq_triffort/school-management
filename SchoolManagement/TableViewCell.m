//
//  TableViewCell.m
//  SchoolManagement
//
//  Created by Macwaves on 4/15/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
