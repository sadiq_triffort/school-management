//
//  MyCell.h
//  SchoolManagement
//
//  Created by Macwaves on 1/21/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UILabel *textLabel;


@end
