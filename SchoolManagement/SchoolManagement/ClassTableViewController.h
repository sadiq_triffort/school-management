//
//  ClassTableViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 1/9/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Database.h"

@interface ClassTableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideButton;

@property(nonatomic,strong) IBOutlet UITableView *tableView;

@property(nonatomic,strong) NSMutableArray *classList;

@property(nonatomic,strong) Database *studentData;

@property(nonatomic) int cls_id;
@property(nonatomic) int s_id;

@end
