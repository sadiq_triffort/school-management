
//  MenuTableViewController.m
//  SchoolManagement
//  Created by Macwaves on 12/26/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.

#import "MenuTableViewController.h"
#import "DetailViewController.h"
#import "SWRevealViewController.h"
#import "NewClassViewController.h"
#import "ClassDetailsViewController.h"
#import "StudentDetailsViewController.h"
#import "StudentDetails.h"

@interface MenuTableViewController (){
    NSArray *menuItems;
    StudentDetails *st;
}
@end

@implementation MenuTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"SCHOOL App";
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MenuList" ofType:@"plist"];
    
    menuItems = [[NSArray alloc] initWithContentsOfFile:path];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images-3.jpeg"]];
    
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = [menuItems objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [menuItems objectAtIndex:indexPath.row];
    
    cell.textLabel.font =[ UIFont fontWithName:@"TrebuchetMS-Bold" size:22];
    
    cell.textLabel.textColor = [UIColor darkTextColor];
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    return cell;
    
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    
 NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
 UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    
 destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];
    
 if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
     
 SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
     
 swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
     
 UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
     
 [navController setViewControllers: @[dvc] animated: NO ];
     
 [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
     
};
     
}
    
}
@end
