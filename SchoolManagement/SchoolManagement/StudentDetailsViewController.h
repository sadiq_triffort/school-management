//
//  StudentDetailsViewController.h
//  SchoolManagement
//  Created by Macwaves on 1/9/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.

#import <UIKit/UIKit.h>
#import "Database.h"

@interface StudentDetailsViewController : UITableViewController<UISearchDisplayDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate>

@property(nonatomic,strong) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(strong,nonatomic) IBOutlet UITableView *customTable;

@property(nonatomic,strong) NSMutableArray  *studentList;
@property(nonatomic,strong) Database *studentData;

@property(nonatomic) int c_id;
@property (nonatomic) int studentID;

@property(nonatomic,strong) StudentDetailsViewController *stdent;

@property(nonatomic,strong) NSString *claNameForTitle;

@end
