//
//  ClassTableViewController.m
//  SchoolManagement
//  Created by Macwaves on 1/9/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.


#import "ClassTableViewController.h"
#import "SWRevealViewController.h"
#import "StudentDetails.h"
#import "StudentDetailsViewController.h"

@interface ClassTableViewController (){
    
    StudentDetails *std;
}

@end

@implementation ClassTableViewController

#pragma mark DidLoad
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title =@"Update";
    
     self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.sideButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu.png"] style:UIBarButtonItemStylePlain target:self action:@selector(pp:)];
    
    self.navigationItem.leftBarButtonItem = self.sideButton;
    
    self.studentData = [[Database alloc] init];
    
    [self.studentData initDatabase];
    
    self.classList = [self.studentData getClass];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images-3.jpeg"]];
    
    [self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated{
    
     self.editButtonItem.title = @"DELETE CLASS";
}

#pragma mark IBAction
-(IBAction)pp:(id)sender{
    
   self.sideButton.tintColor = [UIColor colorWithWhite:0.1f alpha:0.9f];
    
   self.sideButton.target = self.revealViewController;
    
   self.sideButton.action = @selector(revealToggle:);
    
   [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    StudentDetails *student= [_classList objectAtIndex:indexPath.row];
    
    [_studentData deleteClass:student];
    
    [_classList removeObjectAtIndex:indexPath.row];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Table Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.classList.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    std = [self.classList objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",std.className];
    
    cell.textLabel.font =[ UIFont fontWithName:@"TrebuchetMS-Bold" size:22];
    
    cell.textLabel.textColor = [UIColor darkTextColor];
    
    [self.tableView setSeparatorColor:[UIColor grayColor]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StudentDetails *st= [self.classList objectAtIndex:indexPath.row];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    StudentDetailsViewController *studentDetail = (StudentDetailsViewController*)[mainStoryboard
                                                                instantiateViewControllerWithIdentifier: @"StudentDetailsViewController"];
    
    [studentDetail  setC_id:st.class_id];
    studentDetail.claNameForTitle=st.className;
    
    [self.navigationController pushViewController:studentDetail animated:YES];
    
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    std = [self.classList objectAtIndex:indexPath.row];
    
    if (std.class_id > 0) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"UPDATE CLASS NAME" message:@"ENTER" preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField1)
         
         {
             
             textField1.text = std.className;
             
         }];
        
               UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"CANCEL", @"Cancel action")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"UPDATE", @"Update")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       if (std.class_id > 0) {
                                           UITextField *namefield = [alertController.textFields objectAtIndex:0];
                                           std.className = namefield.text;
                                           [_studentData updateClass:std];
                                           [self.tableView reloadData];
                                       }
                                   }];
        
        [alertController addAction:cancelAction];
        
        [alertController addAction:okAction];
        
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
        
    }
     
}

@end
