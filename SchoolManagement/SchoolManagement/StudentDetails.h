//
//  StudentDetails.h
//  sdf
//
//  Created by Macwaves on 12/23/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StudentDetails : NSObject
@property (nonatomic) int identity;
@property (nonatomic) int studentId;
@property (nonatomic,strong) NSString *className;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *fathersName;
@property (nonatomic) NSString* EmailIdStudent;
@property(nonatomic,strong) NSString *s_phn;
@property(nonatomic,strong) NSString *f_phn;
@property(nonatomic,strong) NSString *s_Address;
@property(nonatomic) int  class_id;
@property(nonatomic,strong) NSString *maths;
@property(nonatomic,strong) NSString *physics;
@property(nonatomic,strong) NSString *chemistry;
@property(nonatomic,strong) NSString *hindi;
@property(nonatomic,strong) NSString *english;
@property(nonatomic) int s_id;
@property(nonatomic) int cls_id;
@property(nonatomic) int m;
@property(nonatomic) int p;
@property(nonatomic) int c;
@property(nonatomic) int h;
@property(nonatomic) int e;
@end
