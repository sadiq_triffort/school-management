//
//  StudentRecordViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 1/29/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"
#import "UIImage+FlatUI.h"

@interface StudentRecordViewController : UIViewController
@property(nonatomic) int s_id;
@property(nonatomic) int c_id;
@property(nonatomic) int identity;
@property(nonatomic,strong) IBOutlet UITextField *maths;
@property(nonatomic,strong) IBOutlet UITextField *physics;
@property(nonatomic,strong) IBOutlet UITextField *chemistry;
@property(nonatomic,strong) IBOutlet UITextField *hindi;
@property(nonatomic,strong) IBOutlet UITextField *english;
@property (weak, nonatomic) IBOutlet FUIButton *insertRecord;
-(IBAction)addRecord:(id)sender;
@end
