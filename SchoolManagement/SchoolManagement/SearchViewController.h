//
//  SearchViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 1/14/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Database.h"
#import "FUIButton.h"

@interface SearchViewController : UIViewController{
    
    IBOutlet UITextField *name;
    IBOutlet UITextField *fatherName;
    IBOutlet UITextField *emailid;
    IBOutlet UITextField *std_phn;
    IBOutlet UITextField *father_phn;
    IBOutlet UITextField *address;
 
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideButton;
@property(nonatomic) int studentidentification;
@property(nonatomic,strong) Database *datab;
@property (weak, nonatomic) IBOutlet FUIButton *Search;
-(IBAction)findData:(id)sender;

@end
