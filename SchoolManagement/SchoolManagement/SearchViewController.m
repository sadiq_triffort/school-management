//
//  SearchViewController.m
//  SchoolManagement
//
//  Created by Macwaves on 1/14/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.

#import "SearchViewController.h"
#import "SWRevealViewController.h"
#import "StudentDetails.h"
#import "UIColor+FlatUI.h"

@interface SearchViewController (){
    NSArray *array;
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sideButton.tintColor = [UIColor colorWithWhite:0.1f alpha:0.9f];
    self.sideButton.target = self.revealViewController;
    self.sideButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    self.Search.buttonColor = [UIColor turquoiseColor];
    self.Search.shadowColor = [UIColor greenSeaColor];
    self.Search.shadowHeight = 3.0f;
    self.Search.cornerRadius = 6.0f;
    [self.Search setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.Search setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)findData:(id)sender{
    self.datab = [[Database alloc] init];
    [self.datab initDatabase];
    StudentDetails *stdnt = [[StudentDetails alloc] init];
    stdnt =  [_datab studentwithStudentid];
    _studentidentification =stdnt.studentId;
//   NSArray *data = [_datab getStudentWithsId:_studentidentification];
//    NSLog(@"%@",data);
//    if (data == nil) {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
//                              @"Data not found" message:nil delegate:nil cancelButtonTitle:
//                              @"OK" otherButtonTitles:nil];
//        [alert show];
//        name.text =@"";
//        fatherName.text = @"";
//        emailid.text =@"";
//        std_phn.text =@"";
//        father_phn.text =@"";
//        address.text =@"";
//    }
//    else{
//        name.text =[data objectAtIndex:0];
//        fatherName.text = [data objectAtIndex:1];
//        emailid.text =[data objectAtIndex:2];
//        std_phn.text = [data objectAtIndex:3];
//        father_phn.text = [data objectAtIndex:4];
//        address.text =[data objectAtIndex:5];
//    }
//}
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
