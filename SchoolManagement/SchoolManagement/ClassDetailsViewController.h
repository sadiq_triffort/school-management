//
//  ClassDetailsViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 12/30/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideButton;
@property(strong,nonatomic) IBOutlet UITableView *tableView;
@end
