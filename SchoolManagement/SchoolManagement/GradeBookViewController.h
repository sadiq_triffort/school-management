//
//  GradeBookViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 1/20/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Database.h"

@interface GradeBookViewController : UIViewController
@property(nonatomic,strong) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property(nonatomic,strong) NSMutableArray *classList;
@property(nonatomic,strong) Database *studentData;
@property(nonatomic) int cls_id;
@property(nonatomic) int s_id;

@end
