//
//  main.m
//  SchoolManagement
//
//  Created by Macwaves on 12/26/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
