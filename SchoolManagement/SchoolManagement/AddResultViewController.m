//
//  AddResultViewController.m
//  SchoolManagement
//
//  Created by Macwaves on 1/29/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.



#import "AddResultViewController.h"
#import "StudentDetails.h"
#import "StudentViewController.h"
#import "SWRevealViewController.h"

@interface AddResultViewController (){
    
    NSArray *classList;
    NSArray *array;
    
    StudentDetails *std;
    
}

@end

@implementation AddResultViewController

@synthesize maths,physics,chemistry,hindi,english;


#pragma mark  DidLoad
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title =@"Select Class";
    
    self.sidebarButton.tintColor = [UIColor colorWithWhite:0.1f alpha:0.9f];
    
    self.sidebarButton.target = self.revealViewController;
    
    self.sidebarButton.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.studentDat = [[Database alloc] init];
    
    [self.studentDat initDatabase];
    
    self->classList = [self.studentDat getClass];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background_815.jpg"]];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark  TableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return classList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    std = [classList objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",std.className];
    
    cell.textLabel.font =[UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    
    cell.textLabel.textColor = [UIColor darkTextColor];
    
    [self.tableView setSeparatorColor:[UIColor grayColor]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    StudentDetails *st= [classList objectAtIndex:indexPath.row];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    StudentViewController *studentDetail = (StudentViewController*)[mainStoryboard
                                                                instantiateViewControllerWithIdentifier: @"StudentViewController"];
    
    [studentDetail  setC_id:st.class_id];
    studentDetail.classNameforTitle=st.className;
    
    [self.navigationController pushViewController:studentDetail animated:YES];
    
}


@end
