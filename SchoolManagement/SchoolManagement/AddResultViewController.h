//
//  AddResultViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 1/29/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Database.h"

@interface AddResultViewController : UIViewController

@property(nonatomic,strong) IBOutlet UITextField *maths,*physics,*chemistry,*hindi,*english;
@property(nonatomic,strong) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property(nonatomic) int s_id;

@property(nonatomic,strong) Database *studentDat;

@end
