//
//  AddStudentViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 12/31/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Database.h"
#import "NewClassViewController.h"

@class TPKeyboardAvoidingScrollView;

@interface AddStudentViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideButton;
@property(nonatomic,strong) IBOutlet UITableView  *tableView;
@property(nonatomic,strong) IBOutlet UIButton *drp;
@property(nonatomic,strong) IBOutlet UITextField *name,*fathersName,*email,*s_phn,*f_phn,*s_Address;
@property (nonatomic, retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;


@property(nonatomic,strong) NSMutableArray *classList;
@property(nonatomic,strong) NSMutableArray *studentList;

@property(nonatomic,strong) Database *studentData;
@property (strong, nonatomic) NewClassViewController *editViewController;
@property (strong, nonatomic) Database *studentDbUtil;

@property (nonatomic) NSInteger studentIdent;



-(IBAction)dropDown:(id)sender;
-(IBAction)save:(id)sender;
-(IBAction)Cancel:(id)sender;

@end
