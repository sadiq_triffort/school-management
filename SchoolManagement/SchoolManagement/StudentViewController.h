//
//  StudentViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 1/29/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentViewController : UIViewController

@property(nonatomic) int c_id;
@property(nonatomic) int studentIdentification;

@property(nonatomic,strong) IBOutlet UITableView *tableView;

@property(nonatomic,strong) NSMutableArray  *studentList;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property(nonatomic,strong) IBOutlet NSString *classNameforTitle;


@end
