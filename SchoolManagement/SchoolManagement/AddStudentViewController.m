//
//  AddStudentViewController.m
//  SchoolManagement
//  Created by Sadiq on 12/31/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.


#import "AddStudentViewController.h"
#import "SWRevealViewController.h"
#import "Database.h"
#import "StudentDetails.h"
#import "AddResultViewController.h"
#import "TPKeyboardAvoidingScrollView.h"


@interface AddStudentViewController (){
    
    BOOL flag;
    BOOL isSelected;
    
    int class_identity;
    
    StudentDetails *std;
    
    sqlite3 *sqlieDB;
    
    NSMutableArray *array;
    NSString *str;
    
}

@end

@implementation AddStudentViewController

@synthesize classList,name,fathersName,email,studentIdent,studentData,studentDbUtil;


#pragma mark  DiDLoad
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    NSString *imageName = @"images-3.jpeg";
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:imageName]]];
    
    self.studentData = [[Database alloc] init];
    
    [studentData initDatabase];
    
    self.sideButton.tintColor = [UIColor colorWithWhite:0.1f alpha:0.9f];
    
    self.sideButton.target = self.revealViewController;
    
    self.sideButton.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background_815.jpg"]];
    
     [self.scrollView contentSizeToFit];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark  ViewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.classList = [studentData  getClass];
    
    [self.tableView reloadData];
    
    self.tableView.hidden = YES;
}

#pragma mark  TableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.classList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier =@"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    cell.backgroundColor = [UIColor clearColor];
    
    std = [self.classList objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",std.className];
    
    cell.textLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cll = [tableView cellForRowAtIndexPath:indexPath];
    
    [self.drp setTitle:cll.textLabel.text forState:UIControlStateNormal];
    
    self.tableView.hidden =YES;
    
    StudentDetails *st= [self.classList objectAtIndex:indexPath.row];
    
    class_identity=st.class_id;
    
       isSelected = TRUE;
}


#pragma mark  IBAction
- (IBAction)dropDown:(UIButton *)sender{
    
    self.tableView.hidden =YES;
    
    if (isSelected ==TRUE) {
        
        if (flag==1) {
            
            flag=0;
            
            self.tableView.hidden=YES;
        }
        
        else{
            
            flag=1;
            
            self.tableView.hidden=NO;
            
        }
    }
    
    self.tableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStylePlain];
    
    [self.tableView setFrame:CGRectMake(38, 95, 240, 320)];
    
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    self.tableView.backgroundColor =[UIColor clearColor];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background_815.jpg"]];
    
    self.tableView.dataSource = self;
    
    self.tableView.delegate = self;
    
    self.tableView.layer.cornerRadius=5.0f;
    
    [self.view addSubview:self.tableView];
    
    if (flag==1) {
        
        flag=0;
        
        self.tableView.hidden=YES;
        
        }
    
    else{
        
         flag=1;
        
        self.tableView.hidden=NO;
        
    }
}


-(IBAction)save:(id)sender{
    
    self.studentDbUtil = [[Database alloc] init];
    
    [studentDbUtil initDatabase];
    
    if ([name.text isEqualToString:@""] || [_drp.titleLabel.text isEqualToString:@"Select Class"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"Text Field Empty" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        
        [alertView show];
        
    }
    
    else if ([fathersName.text isEqualToString:@""] || [_drp.titleLabel.text isEqualToString:@"Select Class"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"Text Field Empty" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    
    else if ([_s_phn.text isEqualToString:@""] || [_drp.titleLabel.text isEqualToString:@"Select Class"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"Text Field Empty" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    
    else if ([_f_phn.text isEqualToString:@""] || [_drp.titleLabel.text isEqualToString:@"Select Class"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"Text Field Empty" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    
    else if ([_s_Address.text isEqualToString:@""] || [_drp.titleLabel.text isEqualToString:@"Select Class"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"Text Field Empty" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    
    else{
        
    StudentDetails *studentDetails = [[StudentDetails alloc] init];
        
    studentDetails.className = self.drp.titleLabel.text;
        
    studentDetails.class_id = class_identity;
        
    studentDetails.name = self.name.text;
        
    studentDetails.fathersName = self.fathersName.text;
        
    studentDetails.EmailIdStudent = self.email.text;
        
    studentDetails.s_phn = self.s_phn.text;
        
    studentDetails.f_phn = self.f_phn.text;
        
    studentDetails.s_Address = self.s_Address.text;
        
    [studentDbUtil addStudent:studentDetails];
        
    self.name.text = nil;
        
    self.fathersName.text = nil;
        
    self.email.text = nil;
        
    self.s_phn.text = nil;
        
    self.f_phn.text = nil;
        
    self.s_Address.text = nil;
        
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Student Added Successfully" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
  [alertView show];
        
  StudentDetails *student = [[StudentDetails alloc] init];
        
  student.m =0;
        
  student.p =0;
        
  student.c =0;
        
  student.h =0;
        
  student.e=0;
        
  student.class_id =class_identity;
        
  [studentDbUtil adding:studentDetails];
        
    }
}

- (IBAction)Cancel:(id)sender{
    
    self.name.text = nil;
    
    self.fathersName.text = nil;
    
    self.email.text = nil;
    
    self.s_phn.text = nil;
    
    self.f_phn.text = nil;
    
    self.s_Address.text = nil;
}

#pragma mark TextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.tableView.hidden=YES;
}


@end
