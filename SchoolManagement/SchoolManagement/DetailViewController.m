
//  DetailViewController.m
//  SchoolManagement
//  Created by Macwaves on 12/26/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.


#import "DetailViewController.h"
#import "SWRevealViewController.h"

@interface DetailViewController ()
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Home";
    
    self.sidebarButton.tintColor = [UIColor colorWithWhite:0.1f alpha:0.9f];
    
    self.sidebarButton.target = self.revealViewController;
    
    self.sidebarButton.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lessons_256.png"]];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

@end
