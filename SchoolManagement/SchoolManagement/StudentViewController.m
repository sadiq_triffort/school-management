//
//  StudentViewController.m
//  SchoolManagement
//  Created by Macwaves on 1/29/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.

#import "StudentViewController.h"
#import "StudentDetails.h"
#import "Database.h"
#import "CustomResultViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface StudentViewController ()<MJSecondPopupDelegate,UISearchDisplayDelegate>{
    
    Database *studentData;
    
    NSArray *array;
    
    StudentDetails *student;
    
     BOOL isFiltered;
    
     NSMutableArray *searchResults;
}

@end

@implementation StudentViewController

@synthesize studentList;


#pragma mark  DiDLoad
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = _classNameforTitle;
    
     _searchBar.delegate = (id)self;
    
    studentData = [[Database alloc] init];
    
    [studentData initDatabase];
    
    studentList = [studentData getStudentWithClsId:_c_id];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background_815.jpg"]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismsKeyboard)];
    
    [self.tableView addGestureRecognizer:tap];
    
}

- (void) dismsKeyboard
{
    [_searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark  TableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(isFiltered){
        return searchResults.count;
    }
    else{
        return self.studentList.count;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    
    StudentDetails *st= [self.studentList objectAtIndex:indexPath.row];
    
    
    if(isFiltered){
        
        student = [searchResults objectAtIndex:indexPath.row];
        
        cell.textLabel.text= [NSString stringWithFormat:@"%@",student.name];
        
        cell.textLabel.font =[ UIFont fontWithName:@"TrebuchetMS-Bold" size:22];
        
        cell.textLabel.textColor = [UIColor darkTextColor];
        
        [self.tableView setSeparatorColor:[UIColor grayColor]];
        
    }

    else{
    cell.textLabel.text = [NSString stringWithFormat:@"%@",st.name];
    
    cell.textLabel.font =[ UIFont fontWithName:@"TrebuchetMS-Bold" size:22];
    
    cell.textLabel.textColor = [UIColor darkTextColor];
    
    [self.tableView setSeparatorColor:[UIColor grayColor]];
    
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    if (isFiltered) {
        StudentDetails* std = [searchResults objectAtIndex:indexPath.row];
        array = [studentData getRecrdwithClassId:std.studentId];
            
        student= [array objectAtIndex:0];
        student.s_id = std.studentId;
        student.cls_id = _c_id;
        student.name=std.name;
        
        [self presentController];
        
    }
        else{
            
            StudentDetails* std = [self.studentList objectAtIndex:indexPath.row];
            
            array = [studentData getRecrdwithClassId:std.studentId];
            
            student= [array objectAtIndex:0];

            student.s_id = std.studentId;
            
            student.cls_id = _c_id;
            
            student.name=std.name;
            
            [self presentController];
            
        }

}

-(void)presentController{
    
    CustomResultViewController *secondDetailViewController = [[CustomResultViewController alloc] initWithNibName:@"CustomResultViewController" bundle:nil];
    
    secondDetailViewController.delegate = self;
    
    secondDetailViewController.studentInfo=student;
    
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationSlideBottomTop];
    
}

- (void)cancelButtonClicked:(CustomResultViewController *)aSecondDetailViewController
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        
        searchResults = [[NSMutableArray alloc] init];
        
        for ( StudentDetails* studen in studentList)
        {
            NSRange nameRange = [studen.name rangeOfString:text options:NSCaseInsensitiveSearch];
            
            NSRange descriptionRange = [studen.description rangeOfString:text options:NSCaseInsensitiveSearch];
            
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
            {
                [searchResults addObject:studen];
            }
        }
    }
    
    [self.tableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return _classNameforTitle;
}


@end
