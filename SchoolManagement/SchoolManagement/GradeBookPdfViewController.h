//
//  GradeBookPdfViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 2/2/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreText/CoreText.h>
#import <MessageUI/MessageUI.h>

@interface GradeBookPdfViewController : UIViewController<MFMailComposeViewControllerDelegate>
@property(nonatomic,strong) NSString *pdf;
@property(nonatomic,strong) IBOutlet UIWebView *web;
- (IBAction)showEmail:(id)sender;
@end
