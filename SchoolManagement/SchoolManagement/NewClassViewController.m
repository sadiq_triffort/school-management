//
//  NewClassViewController.m
//  SchoolManagement
//  Created by Macwaves on 12/26/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.

#import "NewClassViewController.h"
#import "SWRevealViewController.h"
#import "StudentDetails.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface NewClassViewController (){
    
    NSMutableArray *array;
    
}

@end

@implementation NewClassViewController

#pragma mark  DiDLoad
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    NSString *imageName = @"images-3.jpeg";
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:imageName]]];
    
    self.sideButton.tintColor = [UIColor colorWithWhite:0.1f alpha:0.9f];
    
    self.sideButton.target = self.revealViewController;
    
    self.sideButton.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.scrollView contentSizeToFit];
    
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

#pragma mark  IBAction
-(IBAction)submitButton:(id)sender{
    
    self.studentDb = [[Database alloc] init];
    
    [self.studentDb initDatabase];
    
    array =[_studentDb getClass];
    
    NSArray *arr = [array valueForKey:@"className"];
    
    StudentDetails *student = [[StudentDetails alloc] init];
    
    if ([_className.text isEqualToString:@""]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"EMPTY TEXT FIELD" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        
    }
    
    else if ([_name.text isEqualToString:@""]){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"EMPTY TEXT FIELD" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        
    }
    
    else if ([_fathersName.text isEqualToString:@""]){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"EMPTY TEXT FIELD" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    
    else if ([_EmailIdStudent.text isEqualToString:@""]){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"EMPTY TEXT FIELD" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        
    }
    
    else if ([_s_phn.text isEqualToString:@""]){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"EMPTY TEXT FIELD" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
    }
    
    else if ([_f_phn.text isEqualToString:@""]){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"EMPTY TEXT FIELD" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        
    }
    
    else if ([_s_Address.text isEqualToString:@""]){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION!" message:@"EMPTY TEXT FIELD" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        
    }
    
    else{
        
    student.className = self.className.text;
        
    student.name = self.name.text;
        
    student.fathersName = self.fathersName.text;
        
    student.EmailIdStudent = self.EmailIdStudent.text;
        
    student.s_phn = self.s_phn.text;
        
    student.f_phn = self.f_phn.text;
        
    student.s_Address = self.s_Address.text;
        
        BOOL wordFound = NO;
        
        for (int i=0; i<[arr count]; i++) {
            
        NSString *str =[arr objectAtIndex:i];
            
        if ([self.className.text isEqualToString:str]) {
            
            UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"CLASS ALREADY EXIST" message:@"Enter Another Class Name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [alertView show];
            
             wordFound  =YES;
        }
            
        }
        if (wordFound ==NO) {
            
            [self.studentDb saveStudentDetails:student];
            
            self.className.text = nil;
            
            self.name.text = nil;
            
            self.fathersName.text = nil;
            
            self.EmailIdStudent.text = nil;
            
            self.s_phn.text = nil;
            
            self.f_phn.text = nil;
            
            self.s_Address.text = nil;
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"CLASS" message:@"STUDENT ADDED" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            
        }
    }
}

-(IBAction)clear:(id)sender{
    
    self.className.text = nil;
    
    self.name.text = nil;
    
    self.fathersName.text = nil;
    
    self.EmailIdStudent.text = nil;
    
    self.s_phn.text = nil;
    
    self.f_phn.text = nil;
    
    self.s_Address.text = nil;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
    
}

@end
