//
//  GradeViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 1/20/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.

#import <UIKit/UIKit.h>
#import "Database.h"
#import "XCMultiSortTableView.h"
#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>

@interface GradeViewController : UIViewController
@property(nonatomic) int c_id;
@property(nonatomic) int s_id;
@property(nonatomic,strong) NSMutableArray  *studentList;
@property(nonatomic,strong) Database *studentData;
@property (nonatomic) int classID;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property(nonatomic,strong) IBOutlet UICollectionView *collectionView;
@property(nonatomic,strong) IBOutlet UILabel *textlabel;
@property(nonatomic,strong) NSMutableArray *array;
@end
