
//  GradeBookPdfViewController.m
//  SchoolManagement
//  Created by Macwaves on 2/2/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.

#import "GradeBookPdfViewController.h"

@interface GradeBookPdfViewController ()

@end

@implementation GradeBookPdfViewController

@synthesize pdf,web;

- (void)viewDidLoad

{
    [super viewDidLoad];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"Email" style:UIBarButtonItemStylePlain target:self action:@selector(showEmail:)];
    
    self.navigationItem.rightBarButtonItem = barButton;
    
    NSURL *targetURL = [NSURL URLWithString:pdf];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    [web setScalesPageToFit:YES];
    
    [web loadRequest:request];
    
    [web setScalesPageToFit:YES];
   
}

-(void)viewDidAppear:(BOOL)animated{
    
}

- (IBAction)showEmail:(id)sender {
    
    NSString *emailTitle = @"Grade Book";
    
     NSData * pdfData = [NSData dataWithContentsOfFile:pdf];
    
    NSArray *toRecipents = [NSArray arrayWithObject:@"sadique.hussain@triffort.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    
    mc.mailComposeDelegate = self;
    
    [mc setSubject:emailTitle];
    
    [mc addAttachmentData:pdfData mimeType:@"application/pdf" fileName:@"Sad.pdf"];
    
    [mc setMessageBody:@"Pdf" isHTML:NO];
    
    [mc setToRecipients:toRecipents];
    
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
        [self dismissViewControllerAnimated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
