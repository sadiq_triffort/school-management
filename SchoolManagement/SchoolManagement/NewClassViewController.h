//
//  NewClassViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 12/26/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.

#import <UIKit/UIKit.h>
#import "Database.h"
#import "SWRevealViewController.h"

@class TPKeyboardAvoidingScrollView;

@interface NewClassViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideButton;
@property(nonatomic,strong) IBOutlet UITextField *className,*name,*fathersName,*EmailIdStudent,*s_phn,*f_phn,*s_Address;
@property (nonatomic, retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (strong, nonatomic) Database *studentDb;

-(IBAction)submitButton:(id)sender;
-(IBAction)clear:(id)sender;

@end
