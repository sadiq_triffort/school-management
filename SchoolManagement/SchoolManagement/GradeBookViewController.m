//
//  GradeBookViewController.m
//  SchoolManagement
//  Created by Macwaves on 1/20/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.


#import "GradeBookViewController.h"
#import "SWRevealViewController.h"
#import "GradeViewController.h"

@interface GradeBookViewController (){
    StudentDetails *std;
}
@end

@implementation GradeBookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title =@"Class List";
    
    self.sidebarButton.tintColor = [UIColor colorWithWhite:0.1f alpha:0.9f];
    
    self.sidebarButton.target = self.revealViewController;
    
    self.sidebarButton.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.studentData = [[Database alloc] init];
    
    [self.studentData initDatabase];
    
    self.classList = [self.studentData getClass];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background_815.jpg"]];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.classList.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    std = [self.classList objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",std.className];
    
    cell.textLabel.font =[UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    
    cell.textLabel.textColor = [UIColor darkTextColor];
    
    [self.tableView setSeparatorColor:[UIColor grayColor]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    StudentDetails *st= [self.classList objectAtIndex:indexPath.row];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    GradeViewController *studentDetail = (GradeViewController*)[mainStoryboard
                                                                instantiateViewControllerWithIdentifier: @"GradeViewController"];
    
    [studentDetail  setC_id:st.class_id];
    
    [self.navigationController pushViewController:studentDetail animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
