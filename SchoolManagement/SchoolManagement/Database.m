//
//  Database.m
//  Created by Macwaves on 12/23/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.


#import "Database.h"
#import "StudentDetails.h"
#import "NewClassViewController.h"

@implementation Database


#pragma mark  INITIALIZE DATABASE
-(void)initDatabase
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dbName = @"sql.sqlite";
    self.databasePath = [documentsDirectory stringByAppendingFormat:@"%@%@",@"/",dbName];
    if(![fileManager fileExistsAtPath:_databasePath])
    {
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dbName];
    [fileManager copyItemAtPath:databasePathFromApp toPath:_databasePath error:nil];
    }
}


#pragma mark  Update Class
- (void) updateClass :(StudentDetails *) student
{
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        if (student.class_id > 0) {
            NSLog(@"Exitsing data, Update Please");
            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE Class set  classname = '%@' WHERE ID = ?",
                                   student.className];
            const char *update_stmt = [updateSQL UTF8String];
            sqlite3_prepare_v2(sqlieDB, update_stmt, -1, &statement, NULL);
            sqlite3_bind_int(statement, 1, student.class_id);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                success = true;
            }
        }
    }
}


#pragma mark  Delete Class
- (BOOL) deleteClass:(StudentDetails *)student
{
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [_databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        if (student.class_id > 0) {
            
            NSLog(@"Exitsing data, Delete Please");
            NSString *deleteSQL = [NSString stringWithFormat:@"DELETE  from Class WHERE ID = %d",student.class_id];
            const char *delete_stmt = [deleteSQL UTF8String];
            sqlite3_prepare_v2(sqlieDB, delete_stmt, -1, &statement, NULL );
            sqlite3_bind_int(statement, 1, student.class_id);
            if (sqlite3_step(statement) == SQLITE_DONE)
                
            {
                success = true;
            }
            
            NSString *delete = [NSString stringWithFormat:@"DELETE  from Student WHERE class_id = %d",student.class_id];
            const char *delete_stm = [delete UTF8String];
            sqlite3_prepare_v2(sqlieDB, delete_stm, -1, &statement, NULL );
            sqlite3_bind_int(statement, 1, student.class_id);
            if (sqlite3_step(statement) == SQLITE_DONE)
                
            {
                success = true;
            }
            
            NSString *delet = [NSString stringWithFormat:@"DELETE  from Result WHERE c_ident = %d",student.class_id];
            const char *dele = [delet UTF8String];
            sqlite3_prepare_v2(sqlieDB, dele, -1, &statement, NULL );
            sqlite3_bind_int(statement, 1, student.class_id);
            if (sqlite3_step(statement) == SQLITE_DONE)
                
            {
                success = true;
            }


        }
        
        else{
            
            NSLog(@"New data, Nothing to delete");
            success = true;
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(sqlieDB);
        
    }
    
    return success;
}


- (BOOL) deleteStu:(StudentDetails *)student
{
    
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [_databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        if (student.studentId > 0) {
            
            NSLog(@"Exitsing data, Delete Please");
            NSString *deleteSQL = [NSString stringWithFormat:@"DELETE  from Result WHERE s_id = %d",student.studentId];
            const char *delete_stmt = [deleteSQL UTF8String];
            sqlite3_prepare_v2(sqlieDB, delete_stmt, -1, &statement, NULL );
            sqlite3_bind_int(statement, 1, student.studentId);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                success = true;
            }
            NSString *delSQL = [NSString stringWithFormat:@"DELETE  from Student WHERE s_id = %d",student.studentId];
            const char *dlete_stmt = [delSQL UTF8String];
            sqlite3_prepare_v2(sqlieDB, dlete_stmt, -1, &statement, NULL );
            sqlite3_bind_int(statement, 1, student.studentId);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                success = true;
            }

        }
        else{
            NSLog(@"New data, Nothing to delete");
            success = true;
        }
        sqlite3_finalize(statement);
        sqlite3_close(sqlieDB);
    }
    return success;
}


#pragma mark  Save Student Details
- (BOOL) saveStudentDetails:(StudentDetails *)student
{
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
               NSLog(@"New data, Insert Please");
               NSString *insertStatement = [NSString stringWithFormat:@"INSERT INTO  Class (classname) VALUES (\"%@\")", student.className];
               NSLog(@"%@",student.className);
               char *error;
               if ( sqlite3_exec(sqlieDB, [insertStatement UTF8String], NULL, NULL, &error) == SQLITE_OK)
                {
                self.classid = sqlite3_last_insert_rowid(sqlieDB);
                insertStatement = [NSString stringWithFormat:@"INSERT INTO Student (s_Name,f_Name,e_id,s_phn,f_phn,s_Address,class_id) VALUES ( \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%ld\")",
                               student.name,
                               student.fathersName,
                               student.EmailIdStudent,
                               student.s_phn,
                               student.f_phn,
                               student.s_Address,
                              (long)_classid];
                    if ( sqlite3_exec(sqlieDB, [insertStatement UTF8String], NULL, NULL, &error) == SQLITE_OK)
                {
                    NSLog(@"Student inserted.");
                }
                    NSString *insertString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO Result(maths,physics,chemistry,hindi,english,c_ident) VALUES (%d,%d,%d,%d,%d,%ld);", student.m,
                                              student.p,
                                              student.c,
                                              student.h,
                                              student.e,
                                              (long)_classid];
                    if ( sqlite3_exec(sqlieDB, [insertString UTF8String], NULL, NULL, &error) == SQLITE_OK)
                    {
                        NSLog(@"Result inserted.");
                    }

                else
                {
                    NSLog(@"Error: %s", error);
                }
                }
                else
                {
                NSLog(@"Error: %s", error);
                }
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                success = true;
                }
               sqlite3_finalize(statement);
               sqlite3_close(sqlieDB);
    }
    return success;
}


#pragma mark Add Results Or Update Result
-(void) addResults:(StudentDetails *) student{
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        char *error;
        if (student.s_id > 0) {
            
            NSString *insertString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO Result(maths,physics,chemistry,hindi,english,s_id,c_ident) VALUES (%@,%@,%@,%@,%@,%d,%d);", student.maths,
                                      student.physics,
                                      student.chemistry,
                                      student.hindi,
                                      student.english,
                                      student.s_id,
                                      student.cls_id];
            
            if ( sqlite3_exec(sqlieDB, [insertString UTF8String], NULL, NULL, &error) == SQLITE_OK)
            {
                NSLog(@"Student inserted.");
            }
            else
            {
                NSLog(@"Error: %s", error);
            }
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                success = true;
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(sqlieDB);
    }
}



#pragma mark  Default Result Insertion
-(void) adding:(StudentDetails *) student{
    
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        char *error;
             NSString *insertString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO Result(maths,physics,chemistry,hindi,english,c_ident) VALUES (%d,%d,%d,%d,%d,%d);", student.m,
                                   student.p,
                                   student.c,
                                   student.h,
                                   student.e,
                                   student.class_id];
            if ( sqlite3_exec(sqlieDB, [insertString UTF8String], NULL, NULL, &error) == SQLITE_OK)
            {
                NSLog(@"Student inserted.");
            }
            else
            {
                NSLog(@"Error: %s", error);
            }
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                success = true;
            }
         
            sqlite3_finalize(statement);
            sqlite3_close(sqlieDB);
       }
       }


#pragma mark  Add Student
- (void) addStudent:(StudentDetails *) student{
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        NSLog(@" Insert Please");
        char *error;
        if (student.class_id > 0) {
            NSString    *insertStatement = [NSString stringWithFormat:@"INSERT INTO Student (s_Name,f_Name,e_id,s_phn,f_phn,s_Address,class_id) VALUES ( \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\",\"%d\")",
                                            student.name,
                                            student.fathersName,
                                            student.EmailIdStudent,
                                            student.s_phn,
                                            student.f_phn,
                                            student.s_Address,
                                            student.class_id];
            if ( sqlite3_exec(sqlieDB, [insertStatement UTF8String], NULL, NULL, &error) == SQLITE_OK)
            {
                NSLog(@"Student inserted.");
            }
            else
            {
                NSLog(@"Error: %s", error);
            }
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                success = true;
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlieDB);
        }
        }
        }

#pragma mark  Update Student

- (void) updateStudent :(StudentDetails *) student
{
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        if (student.studentId > 0) {
            NSLog(@"Exitsing data, Update Please");
            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE Student set  s_Name = '%@', f_Name = '%@', e_id = '%@',s_phn = '%@' ,f_phn = '%@' ,s_Address = '%@' WHERE s_id = ?",
                                   student.name,
                                   student.fathersName,
                                   student.EmailIdStudent,
                                   student.s_phn,
                                   student.f_phn,
                                   student.s_Address];
            const char *update_stmt = [updateSQL UTF8String];
            sqlite3_prepare_v2(sqlieDB, update_stmt, -1, &statement, NULL);
            sqlite3_bind_int(statement, 1, student.studentId);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                success = true;
            }
            }
    }
}


#pragma mark Get Class
- (NSMutableArray *) getClass
{
    NSMutableArray *className = [[NSMutableArray alloc] init];
    const char *dbpath = [_databasePath UTF8String];
    sqlite3_stmt    *statement;
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        NSString *querySQL = @"SELECT ID, classname  FROM Class";
        const char *query_stmt = [querySQL UTF8String];
        if (sqlite3_prepare_v2(sqlieDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                StudentDetails *st = [[StudentDetails alloc] init];
                st.class_id = sqlite3_column_int(statement, 0);
                st.className = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                [className addObject:st];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(sqlieDB);
    }
    return className;
}


#pragma mark  Get Student By ClassId

-(NSMutableArray *) getStudentWithClsId : (int) classId{
     NSMutableArray *student = [[NSMutableArray alloc] init];
    const char *dbpath =[_databasePath UTF8String];
    sqlite3_stmt *statement;
     if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        NSString *queryStudent = [NSString stringWithFormat:@"SELECT s_id, s_Name, f_Name, e_id ,s_phn ,f_phn,s_Address FROM Student where class_id = %d",classId];
        const char *query_stmt = [queryStudent UTF8String];
        if (sqlite3_prepare_v2(sqlieDB, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                StudentDetails *st = [[StudentDetails alloc] init];
                st.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                st.studentId = sqlite3_column_int(statement, 0);
                st.fathersName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                st.EmailIdStudent = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                st.s_phn = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                st.f_phn = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
                st.s_Address = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
                [student addObject:st];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(sqlieDB);
    }
    return student;
}



#pragma mark  Get Record With ClassId

-(NSMutableArray *) getRecordwithClassId : (int) class_id{
    NSMutableArray *student = [[NSMutableArray alloc] init];
    const char *dbpath =[_databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
        NSString *queryStudent = [NSString stringWithFormat:@"SELECT maths, physics, chemistry, hindi ,english,s_id FROM Result where c_ident = %d",class_id];
        const char *query_stmt = [queryStudent UTF8String];
        if (sqlite3_prepare_v2(sqlieDB, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                StudentDetails *st = [[StudentDetails alloc] init];
                st.maths = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                st.physics = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                st.chemistry = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                st.hindi = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                st.english = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                 st.s_id = sqlite3_column_int(statement, 5);
                [student addObject:st];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(sqlieDB);
    }
    return student;
}

#pragma mark  Get Result with Student Id

-(NSMutableArray *) getRecrdwithClassId : (int) class_id{
    NSMutableArray *student = [[NSMutableArray alloc] init];
    const char *dbpath =[_databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &sqlieDB) == SQLITE_OK)
    {
    NSString *queryStudent = [NSString stringWithFormat:@"SELECT maths, physics, chemistry, hindi ,english FROM Result where s_id = %d",class_id];
        const char *query_stmt = [queryStudent UTF8String];
        if (sqlite3_prepare_v2(sqlieDB, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                StudentDetails *st = [[StudentDetails alloc] init];
                st.maths = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                st.physics = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                st.chemistry = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                st.hindi = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                st.english = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                st.s_id = sqlite3_column_int(statement, 5);
                [student addObject:st];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(sqlieDB);
    }
    return student;
}


@end
