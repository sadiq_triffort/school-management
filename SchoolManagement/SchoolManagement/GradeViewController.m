//
//  GradeViewController.m
//  SchoolManagement
//  Created by Macwaves on 1/20/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.


#import "GradeViewController.h"
#import "MyCell.h"
#import "GradeBookPdfViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface GradeViewController ()<XCMultiTableViewDataSource>

{
    XCMultiTableView *tableView;
}

@property (nonatomic, retain) NSMutableArray *rowsArray;

@end

@implementation GradeViewController

    {
        
    NSMutableArray *headData;
        
    NSMutableArray *leftTableData;
        
    NSMutableArray *rightTableData;
        
    }

@synthesize array;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"Generate Pdf" style:UIBarButtonItemStylePlain target:self action:@selector(push:)];
    
    self.navigationItem.rightBarButtonItem = barButton;
    
    NSString *imageName = @"background_815.jpg";
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:imageName]]];
    
    self.title = @"Grade Book";
    
    self.studentData = [[Database alloc] init];
    
    [self.studentData initDatabase];
    
    self.studentList = [self.studentData getStudentWithClsId:self.c_id];
    
    _classID = self.c_id;
    
    array = [self.studentData getRecordwithClassId:_classID];
    
    [self initData];
    
    tableView = [[XCMultiTableView alloc] initWithFrame:CGRectInset(self.view.bounds, 5.0f, 5.0f)];
    
    tableView.leftHeaderEnable = YES;
    
    tableView.datasource = self;
    
    [self.view addSubview:tableView];
}

#pragma mark IBAction  Push To Pdf

-(IBAction)push:(id)sender{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    GradeBookPdfViewController *grade = (GradeBookPdfViewController*)[mainStoryboard
                                                                    instantiateViewControllerWithIdentifier: @"GradeBookPdfViewController"];
    
    NSString *str;
    
    str = [self createPDFfromUIView:self.view saveToDocumentsWithFileName:@"Sad.pdf"];
    
    grade.pdf= str;
    
    grade.pdf =[self createPDFfromUIView:[self view] saveToDocumentsWithFileName:@"Sad.pdf"];
    
    [self.navigationController pushViewController:grade animated:YES];
    
}


#pragma mark  Grade Book Creation
- (void)initData {
    headData = [[NSMutableArray alloc] initWithObjects:@"Maths",@"Physics",@"Chemistry",@"Hindi",@"English",@"Percentage", nil];
    
    leftTableData = [[NSMutableArray alloc] init];
    
    NSMutableArray *one = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [_studentList count]; i++) {
        
    StudentDetails *st = [_studentList objectAtIndex:i];
        
    [one addObject:[NSString stringWithFormat:@"%@", st.name]];
        
    }
    
    [leftTableData addObject:one];
    
    rightTableData = [[NSMutableArray alloc] init];
    
    NSMutableArray *oneR = [[NSMutableArray alloc] init];
    
     for (int i = 0; i < [array count]; i++) {
         
        StudentDetails *st = [array objectAtIndex:i];
         
        NSMutableArray *ary = [[NSMutableArray alloc] init];
         
        for (int j = 0; j < [headData count]; j++) {
            
            if (j == 0) {
                
        [ary addObject:[NSString stringWithFormat:@"%@", st.maths]];
                
            }
            
            else if (j == 1) {
                
                [ary addObject:[NSString stringWithFormat:@"%@", st.physics]];
                
            }
            else if (j == 2){
                
                [ary addObject:[NSString stringWithFormat:@"%@", st.chemistry]];
                
            }
            else if (j==3){
                
           [ary addObject:[NSString stringWithFormat:@"%@", st.hindi]];
                
            }
            
            else if(j==4){
                
                [ary addObject:[NSString stringWithFormat:@"%@", st.english]];
                
            }
            
            else{
                
                int v1 =st.maths.intValue;
                
                int v2 =st.physics.intValue;
                
                int v3 = st.chemistry.intValue;
                
                int v4 = st.hindi.intValue;
                
                int v5 = st.english.intValue;
                
                int sum;
                
                sum = v1+v2+v3+v4+v5;
                
                int per = (sum *100)/500;
                
                [ary addObject:[NSNumber numberWithInt:per]];
        }
            
        }
         
        [oneR addObject:ary];
         
    }
    
    [rightTableData addObject:oneR];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSArray *)arrayDataForTopHeaderInTableView:(XCMultiTableView *)tableView {
    
    return [headData copy];
    
}
- (NSArray *)arrayDataForLeftHeaderInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    
    return [leftTableData objectAtIndex:section];
    
}

- (NSArray *)arrayDataForContentInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    
    return [rightTableData objectAtIndex:section];
    
}

- (NSUInteger)numberOfSectionsInTableView:(XCMultiTableView *)tableView {
    
    return 1;
    
}

- (CGFloat)tableView:(XCMultiTableView *)tableView contentTableCellWidth:(NSUInteger)column {
    
    if (column == 0) {
        
        return 70.0f;
        
    }
    
    return 70.0f;
    
}

- (CGFloat)tableView:(XCMultiTableView *)tableView cellHeightInRow:(NSUInteger)row InSection:(NSUInteger)section {
    
    if (section == 0) {
        
        return 30.0f;
        
    }
    else {
        
        return 30.0f;
        
    }
    
}


#pragma mark  Create Pdf
-(NSMutableData *)createPDFDatafromUIView:(UIView*)aView
{
    self.view.frame = CGRectMake(0, 0, 540, 2000);
    
    aView = self.view;
    
    NSMutableData *pdfData = [NSMutableData data];
    
    UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil);
    
    UIGraphicsBeginPDFPage();
    
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    CGContextSetInterpolationQuality(pdfContext, kCGInterpolationHigh);
    
    CGContextSetRenderingIntent(pdfContext, kCGRenderingIntentDefault);
    
    [aView.layer renderInContext:pdfContext];
    
    UIGraphicsEndPDFContext();
    
    return pdfData;
}

-(NSString*)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename

{
    
    NSMutableData *pdfData = [self createPDFDatafromUIView:aView];
    
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    
    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
    
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    
    NSLog(@"documentDirectoryFileName: %@",documentDirectoryFilename);
    
    return documentDirectoryFilename;
}


@end
