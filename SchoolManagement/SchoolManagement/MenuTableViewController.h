//
//  MenuTableViewController.h
//  SchoolManagement
//  Created by Sadiq on 12/26/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) UITableView *tableView;

@end
