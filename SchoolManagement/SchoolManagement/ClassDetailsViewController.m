//
//  ClassDetailsViewController.m
//  SchoolManagement
//  Created by Macwaves on 12/30/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.

#import "ClassDetailsViewController.h"
#import "SWRevealViewController.h"
#import "Database.h"

@interface ClassDetailsViewController (){
    
}
@end

@implementation ClassDetailsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.sideButton.tintColor = [UIColor colorWithWhite:0.1f alpha:0.9f];
    self.sideButton.target = self.revealViewController;
    self.sideButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

@end
