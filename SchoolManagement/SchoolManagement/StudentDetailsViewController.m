//
//  StudentDetailsViewController.m
//  SchoolManagement
//  Created by Macwaves on 1/9/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.

#import "StudentDetailsViewController.h"
#import "StudentDetails.h"
#import "TableViewCell.h"
#import "CustomViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface StudentDetailsViewController () <UISearchDisplayDelegate,MJSecondPopupDelegate>{
    
    StudentDetails *student;
    
    NSMutableArray *searchResults;
    NSArray *array;
    
    BOOL isFiltered;
    
}

@end

@implementation StudentDetailsViewController

@synthesize searchBar,c_id;


#pragma mark  DidLoad
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = _claNameForTitle;
    
    self.editButtonItem.title = @"DELETE STUDENT";
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    searchBar.delegate = (id)self;
    
    self.studentData = [[Database alloc] init];
    
    [self.studentData initDatabase];
    
    _studentList = [self.studentData getStudentWithClsId:c_id];
    
    self.table.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images-3.jpeg"]];
    
    array = [[NSArray alloc] initWithObjects:@"Name",@"FName",@"Eid",@"S_phn",@"f_phn",@"Address", nil];
    
    [self.table reloadData];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [self.table reloadData];
}

#pragma mark DeleteStudent
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
      student= [self.studentList objectAtIndex:indexPath.row];
    
      if (student.studentId >0) {
          
      [_studentData deleteStu:student];
          
      [self.studentList removeObjectAtIndex:indexPath.row];
          
      [self.table reloadData];
          
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.customTable.tag==12) {
          return array.count;
        
            }
    
    if(isFiltered){
    return searchResults.count;
    }
    else{
    return self.studentList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    
    StudentDetails *st= [self.studentList objectAtIndex:indexPath.row];
    
    if(isFiltered){
        
        student = [searchResults objectAtIndex:indexPath.row];
        
        cell.textLabel.text= [NSString stringWithFormat:@"%@",student.name];
        
        cell.textLabel.font =[ UIFont fontWithName:@"TrebuchetMS-Bold" size:22];
        
        cell.textLabel.textColor = [UIColor darkTextColor];
        
        [self.tableView setSeparatorColor:[UIColor grayColor]];
        
    }
    
    else{
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@",st.name];
        
        cell.textLabel.font =[ UIFont fontWithName:@"TrebuchetMS-Bold" size:22];
        
        cell.textLabel.textColor = [UIColor darkTextColor];
        
        [self.tableView setSeparatorColor:[UIColor grayColor]];
        
    }
    
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    return cell;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    
   student= [self.studentList objectAtIndex:indexPath.row];
    
    if (isFiltered) {
        
        student = [searchResults objectAtIndex:indexPath.row];
        
        if (student.studentId > 0) {
            
            [self presentController];
            
          }
    }
    else {
    if (student.studentId > 0) {
        
        [self presentController];
        
    }
        
    }
    
    }

-(void)presentController{
    
    CustomViewController *secondDetailViewController = [[CustomViewController alloc] initWithNibName:@"CustomViewController" bundle:nil];
    
    secondDetailViewController.delegate = self;
    
    secondDetailViewController.studentInfo=student;
    
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationSlideBottomTop];
    
}

- (void)cancelButtonClicked:(CustomViewController *)aSecondDetailViewController
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    
    [self.table reloadData];
}



#pragma mark  SearchBar
-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        
        searchResults = [[NSMutableArray alloc] init];
        
        for ( StudentDetails* studen in _studentList)
        {
            NSRange nameRange = [studen.name rangeOfString:text options:NSCaseInsensitiveSearch];
            
            NSRange descriptionRange = [studen.description rangeOfString:text options:NSCaseInsensitiveSearch];
            
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
            {
                [searchResults addObject:studen];
            }
            
        }
    }
    
    [self.table reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return _claNameForTitle;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchreturn
{
    [searchreturn resignFirstResponder];
    // Do the search...
}



@end

