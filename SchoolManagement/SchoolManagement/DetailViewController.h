//
//  DetailViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 12/26/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
