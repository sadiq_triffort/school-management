//
//  Database.h
//  sdf
//
//  Created by Macwaves on 12/23/14.
//  Copyright (c) 2014 Macwaves. All rights reserved.

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "StudentDetails.h"

@interface Database : NSObject{
    
    sqlite3 *sqlieDB;
    
    BOOL isConnected;
}

@property (nonatomic, strong) NSString *databasePath;

@property(nonatomic) NSInteger classid;

- (void) initDatabase;

- (BOOL) saveStudentDetails:(StudentDetails *)student;

- (NSMutableArray *) getClass;

- (void) updateStudent :(StudentDetails *) student;

- (void) addStudent:(StudentDetails *) student;

-(NSMutableArray *) getStudentWithClsId : (int) classId;

-(void) addResults:(StudentDetails *) student;

-(NSMutableArray *) getRecordwithClassId : (int) class_id;

-(NSMutableArray *) getRecrdwithClassId : (int) class_id;

-(void) adding:(StudentDetails *) student;

- (BOOL) deleteClass:(StudentDetails *)student;

- (BOOL) deleteStu:(StudentDetails *)student;

- (void) updateClass :(StudentDetails *) student;

@property(nonatomic,strong) IBOutlet NSString *text;
@end
