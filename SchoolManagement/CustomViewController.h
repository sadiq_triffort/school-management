//
//  CustomViewController.h
//  SchoolManagement
//
//  Created by Macwaves on 4/15/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudentDetails.h"
#import "Database.h"
#import "CustomTextField.h"

@class TPKeyboardAvoidingScrollView;

@protocol MJSecondPopupDelegate;

@interface CustomViewController : UIViewController<UITextFieldDelegate>

@property (assign, nonatomic) id <MJSecondPopupDelegate>delegate;

@property(nonatomic,strong) StudentDetails *studentInfo;

@property(nonatomic,strong) Database *studentData;

@property(nonatomic,strong) IBOutlet UITableView *table;

@property(nonatomic,strong) IBOutlet CustomTextField *name,*f_name,*eid,*s_phn,*f_pn,*s_add;

@property (nonatomic, retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

-(IBAction)upDate:(id)sender;

-(IBAction)Cancel:(id)sender;

@end

@protocol MJSecondPopupDelegate<NSObject>
@optional
- (void)cancelButtonClicked:(CustomViewController*)secondDetailViewController;

@end
