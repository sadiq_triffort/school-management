//
//  XCMultiTableViewBGScrollView.m
//  XCMultiTableDemo


#import "XCMultiSortTableViewBGScrollView.h"
#import "UIView+XCMultiSortTableView.h"

@implementation XCMultiTableViewBGScrollView {
    NSMutableArray *lines;
}

@synthesize parent;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)reDraw {
    if (lines == nil) lines = [[NSMutableArray alloc] initWithCapacity:10];
    for (UIView *view in lines) {
        [view removeFromSuperview];
    }
    [lines removeAllObjects];
    UIView *hidView = [[UIView alloc] initWithFrame:CGRectMake(0.0f - parent.normalSeperatorLineWidth, 0, parent.normalSeperatorLineWidth, self.bounds.size.height)];
    hidView.backgroundColor = parent.normalSeperatorLineColor;
    hidView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
    [self addSubview:hidView];
    [lines addObject:hidView];
    UIView *line = nil;
    CGFloat x = 0.0f;
    NSUInteger columnCount = [parent.datasource arrayDataForTopHeaderInTableView:parent].count;
    for (int i = 0; i < columnCount; i++) {
        CGFloat width;
        if ([parent.datasource respondsToSelector:@selector(tableView:contentTableCellWidth:)]) {
            width = [parent.datasource tableView:parent contentTableCellWidth:i];
        }else {
            width = parent.cellWidth;
        }
        x += width + parent.normalSeperatorLineWidth;
        line = [self addVerticalLineWithWidth:parent.normalSeperatorLineWidth bgColor:parent.normalSeperatorLineColor atX:x];
        [lines addObject:line];
    }
}

@end
